
import axios from "axios"

export const ON_ADD_TASK_ACTION = "ON_ADD_TASK_ACTION ";

export  function AddTaskActionSuccess(task){
    console.log(task)
    return{
        type: ON_ADD_TASK_ACTION,
        task : task,
    }
}

export default function AddTaskAction (value){
    const body ={value}
    return dispatch =>{
        return axios
        .post("http://demo5870593.mockable.io/addtask",body)
        .then(response =>{

            return AddTaskActionSuccess("added")
        })
    }
}